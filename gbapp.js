var path = require("path");
var logger = require("morgan");
var bodyParser = require("body-parser");

var http = require('http').Server(app); 

http.listen(8081, function () {
  console.log('Guestbook app listening on http://127.0.0.1:8081/');
});
app.use(express.static(__dirname + '/assets'));

app.set("views", path.resolve(__dirname, "views")); 
app.set("view engine", "ejs"); 


app.locals.entries = entries; 

app.use(logger("dev"));    
app.use(bodyParser.urlencoded({ extended: false }));


app.get("/", function (request, response) {
  response.render("index");
});
app.get("/new-entry", function (request, response) {
  response.render("new-entry");
});

app.post("/new-entry", function (request, response) {
  if (!request.body.title || !request.body.body) {
    response.status(400).send("Entries must have a title and a body.");
    return;
  }
  entries.push({  // store it
    title: request.body.title,
    content: request.body.body,
    published: new Date()
  });
  response.redirect("/");  
});

app.use(function (request, response) {
  response.status(404).render("404");
});