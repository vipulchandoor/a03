var express = require('express');
var app = express();

var path = require("path");
var logger = require("morgan");
var bodyParser = require("body-parser");
var http = require('http').Server(app);


app.use(express.static(__dirname + '/assets'));

app.set("views", path.resolve(__dirname, "views"));

app.set("assets", path.resolve(__dirname, "assets"));

app.set("view engine", "ejs");

var entries = [];

app.locals.entries = entries;


app.use(logger("dev"));

app.use(bodyParser.urlencoded({ extended: false }));


app.get('/', function (req, res) {

  res.sendFile(path.join(__dirname + '/assets/A01VIP.html'));
});

app.get('/contact', function (req, res) {

  res.sendFile(path.join(__dirname + '/assets/Contact.html'));
});

app.get('/converter', function (req, res) {

  res.sendFile(path.join(__dirname + '/assets/CurrencyConverter.html'));
});

app.get("/index", function (request, response) {

  response.render("index");
});

app.get("/new-entry", function (request, response) {

  response.render("new-entry");
});

app.post("/new-entry", function (request, response) {

  if (!request.body.title || !request.body.body) {

    response.status(400).send("Entries must have a title and a body.");

    return;
  }
  entries.push({

    title: request.body.title,
    content: request.body.body,
    published: new Date()
  });

  response.redirect("/index");
});

app.post("/contact", function (request, response) {

  var api_key = 'key-7ef36e4f4f8591ad5ff24871a6ddbba0';
  var domain = 'sandboxf58419cbb71447df8021e17a13f3c68d.mailgun.org';

  var mailgun = require('mailgun-js')({ apiKey: api_key, domain: domain });
  var details = "Name: " + request.body.firstname +" "+ request.body.lastname + "\nFrom: " + request.body.email + "\n\n Message: " + request.body.body;
  var data = {

    from: 'A03 <postmaster@sandboxf58419cbb71447df8021e17a13f3c68d.mailgun.org>',
    to: "s530459@nwmissouri.edu",
    subject: request.body.subject,
    text: details
  };

  mailgun.messages().send(data, function (error, body) {
    console.log(body);
    if (!error)
      response.send("Mail sent..!!");
    else
      response.send("Mail not sent..!!");
  });
});


// if we get a 404 status, render our 404.ejs view
app.use(function (request, response) {
  
  response.status(404).render("404");
});


// Listen for an application request on port 8081
// use http listen, so we can provide a callback when listening begins
// use the callback to tell the user where to point their browser
app.listen(8081, function () {

  console.log('listening on http://127.0.0.1:8081/');
});
